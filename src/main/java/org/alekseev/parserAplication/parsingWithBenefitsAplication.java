package org.alekseev.parserAplication;

import org.alekseev.parserAplication.domain.Parser;

import org.alekseev.parserAplication.domain.ResultCalculator;
import org.alekseev.parserAplication.domain.WordCounter;
import org.alekseev.parserAplication.validator.Validator;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class parsingWithBenefitsAplication {
    public static void main(String[] args) throws IOException {
        Map<String, Integer> hashMap = new HashMap<String, Integer>();
        System.out.println("the URL:  ");
        Scanner sc = new Scanner(System.in);
        final String URL = sc.nextLine();
        Parser parser = new Parser();
        Validator validator = new Validator();

        ResultCalculator rc = new ResultCalculator(validator, parser);
        String text = rc.makeParse(URL);
        WordCounter wc = new WordCounter(rc);
        hashMap = wc.countWords(text);

        hashMap.forEach((key, value) -> System.out.println(key + " - " + value));
    }
}
