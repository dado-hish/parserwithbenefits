package org.alekseev.parserAplication.domain;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

public class Parser {

    public Document page;

    public String parse(String URL) throws IOException {

        page = Jsoup.parse(new URL(URL), 3000);
        return page.text();
    }
}

