package org.alekseev.parserAplication.domain;

import java.util.*;

public class WordCounter {

    public String[] words;
    private final char[] DELIMITER_CHARS = new char[]{' ', ',', '.', '!', '"', ';', ':', ']', '«', '—'};
    private final ResultCalculator rc;
    Map<String, Integer> result = new HashMap<String, Integer>();

    public WordCounter(ResultCalculator rc){
        this.rc = rc;
    }
    public Map<String, Integer> countWords(String text) {
        String separatorsString;

        words = text.replaceAll("\"", "").split("\\s+");;
        Arrays.sort(words);


        var count = 1;
        for (int i = 0; i < words.length - 1; i++) {

            if (words[i].equals(words[i+1])) {
                count++;
            } else {
                result.put(words[i], count);
                count = 1;
            }
        }
        return result;
    }
}
