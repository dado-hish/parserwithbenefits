package org.alekseev.parserAplication.domain;

import org.alekseev.parserAplication.validator.Validator;

import java.io.IOException;

public class ResultCalculator {

    private final Validator validator;
    private final Parser parser;
    public String text;

    public ResultCalculator(Validator validator, Parser parser){
        this.validator = validator;
        this.parser = parser;
    }

    public String makeParse(String URL) throws IOException {
        validator.validate(URL);
        text = parser.parse(URL);
        return text;
    }

}
